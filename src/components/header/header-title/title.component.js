import { Component } from "react";

class Title extends Component {
    render() {
        return (
            <div className='row text-center'>
                <div className='col-12'>
                    <h1>Chào mừng đến với Devcamp 120</h1>
                </div>
            </div>
        )
    }
}

export default Title;