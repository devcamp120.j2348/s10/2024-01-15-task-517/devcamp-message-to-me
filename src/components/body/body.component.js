import { Component } from "react";
import Input from "./body-input/input.component";
import Output from "./body-output/output.component";

class Body extends Component {
    constructor(props) {
        super(props);

        this.state = {
            inputMessage: "",
            likeDisplay: false,
            outputMessage: []
        }
    }

    updateInputMessage = (message) => {
        this.setState({
            inputMessage: message
        })
    }   

    updateOutputMessage = () => {
        if(this.state.inputMessage) {
            this.setState({
                inputMessage: "",
                likeDisplay: true,
                outputMessage: [...this.state.outputMessage, this.state.inputMessage]
            })
        }   
    }

    render() {
        return (
            <>
                <Input 
                    inputMessageProp={this.state.inputMessage}
                    updateInputMessageProp={this.updateInputMessage}
                    updateOutputMessageProp={this.updateOutputMessage}
                />
                <Output 
                    likeDisplayProp={this.state.likeDisplay}
                    outputMessageProp={this.state.outputMessage}
                />
            </>
        )
    }
}

export default Body;