import { Component } from "react";
import likeImage from "../../../assets/images/like.png";

class Output extends Component {
    render() {
        return (
            <>
                <div className='row mt-3'>
                    <div className='col-12 text-center'>
                        {
                            this.props.likeDisplayProp ?
                                <img alt="title" src={likeImage} width={100}/>
                                : null
                        }
                       
                    </div>
                </div>
                <div className='row mt-3'>
                    <div className='col-12 text-center'>
                        {
                            this.props.outputMessageProp.map((element, index) => {
                                return <p key={index}>{element}</p>
                            })
                        }
                    </div>
                </div>
            </>
        )
    }
}

export default Output;