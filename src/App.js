import 'bootstrap/dist/css/bootstrap.min.css';

import Header from './components/header/header.component';
import Body from './components/body/body.component';

function App() {
  return (
    <div className='container mt-5'>
      {/* Header */}
      <Header />
     
      {/* Body */}
      <Body />
    </div>
  );
}

export default App;
